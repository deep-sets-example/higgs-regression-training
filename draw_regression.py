#!/usr/bin/env python3

"""
Simple script to draw regresison
"""

import argparse
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import colors
from mpl import Canvas
import h5py
import pathlib
import itertools

def get_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('input_file')
    parser.add_argument(
        '-o', '--output-dir', default='plots', type=pathlib.PosixPath)
    parser.add_argument('-n', '--nn', nargs=2,
                        help='nn architecture and weights')
    return parser.parse_args()

def run():
    args = get_args()
    # get the jets out of the input file.
    with h5py.File(args.input_file, 'r') as infile:
        jets = np.asarray(infile['jets'])
        subjets = np.asarray(infile['subjets'])

    if args.nn:
        from train_nn import get_output
        reco = get_output(jets, subjets, *args.nn).flatten()
    else:
        reco = jets['pt']

    reco_vs_truth = np.stack(
        [reco, jets['GhostBHadronsFinalPt']], axis=1)
    hist, edges = np.histogramdd(
        reco_vs_truth, bins=[20,20], range=[(200e3, 550e3), (200e3, 550e3)])
    draw_hist(hist, edges, args.output_dir, is_nn=bool(args.nn))

def draw_hist(hist, edges, out_dir, do_log=True, is_nn=False):
    # make the pt into TeV
    extent = np.asarray([(e[0], e[-1]) for e in edges]).flatten()

    args = dict(
        aspect='auto',
        origin='lower',
        extent=extent*1e-3, # draw in GeV
        interpolation='nearest',
        norm=colors.LogNorm() if do_log else colors.Normalize())

    out_dir.mkdir(parents=True, exist_ok=True)

    name = 'reco_nn' if is_nn else 'reco'

    with Canvas(out_dir / (name + '.pdf')) as can:
        im = can.ax.imshow(hist.T, **args)
        cb = can.fig.colorbar(im)
        can.ax.set_xlabel(r'True $p_{\rm T}$')
        can.ax.set_ylabel(r'Reconstructed $p_{\rm T}$')
        cb.set_label('Number of Jets')

if __name__ == '__main__':
    run()
