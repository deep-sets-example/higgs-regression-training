#!/usr/bin/env python3

"""
Simple script to train a neural network with keras
"""

import argparse
from itertools import product
import numpy as np
from matplotlib import pyplot as plt
import h5py
import os
import json

# grab the same histogram bounds we used in the plotting scripts
from make_hists import BOUNDS

def get_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('input_file')
    parser.add_argument('-e','--epochs', type=int, default=1)
    parser.add_argument('-o','--output-dir', default='model')
    return parser.parse_args()

###############################################################
# Main Training Part
###############################################################

def run():
    args = get_args()

    # get the jets out of the input file.
    with h5py.File(args.input_file, 'r') as infile:
        jets = np.asarray(infile['jets'])
        subjets = np.asarray(infile['subjets'])

    # jets = jets[0:2]
    # subjets = subjets[0:2, :]

    # first, let's make the training dataset!
    jet_inputs, subjet_inputs = preproc_inputs(jets, subjets)
    targets = jets['GhostBHadronsFinalPt']

    # now make the network
    from keras.layers import Input, TimeDistributed, Dense, Softmax, Masking
    from keras.layers import Activation
    from keras.layers import Concatenate
    from SumLayer import SumLayer
    from keras.models import Model

    subjet_node = Input(shape=subjet_inputs.shape[1:], name='subjets')
    tdd = Masking(mask_value=0)(subjet_node)
    for x in range(1):
        tdd = TimeDistributed(Dense(10))(tdd)
        tdd = Activation('relu')(tdd)
    track_sum = SumLayer()(tdd)

    jet_node = Input(shape=jet_inputs.shape[1:], name='jets')
    merged = Concatenate()([jet_node, track_sum])
    for x in range(1):
        dense = Dense(10)(merged)
        merged = Activation('relu')(dense)

    dense2 = Dense(1)(merged)
    model = Model(inputs=[jet_node, subjet_node], outputs=dense2)
    model.compile(optimizer='adam', loss='mse', metrics=['mse'])

    # now fit this thing!
    model.fit([jet_inputs, subjet_inputs], targets, epochs=args.epochs)

    # finally, save the trained network
    odir = args.output_dir
    if not os.path.isdir(odir):
        os.mkdir(odir)
    with open(f'{odir}/architecture.json','w') as arch_file:
        arch_file.write(model.to_json(indent=2))
    model.save_weights(f'{odir}/weights.h5')

    try:
        from keras.utils import plot_model
        plot_model(model, to_file=f'{odir}/model.png')
    except ImportError as err:
        print("failed to import module, won't produce a model.png")

def make_targets(jets):
    labels = jets['GhostBHadronsFinalPt']
    targets = np.stack([labels == x for x in [0, 4, 5]], axis=1)
    return np.asarray(targets, dtype=int)


def get_output(jets, subjets, model_path, weights_path):
    """
    Get the discriminant from a saved model
    """

    # this just silences some annoying warnings that I get from
    # tensorflow. They might be useful for training but in evaluation
    # they should be harmless.
    os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

    # load the model
    from keras.models import model_from_json
    from SumLayer import SumLayer
    with open(model_path,'r') as model_file:
        model = model_from_json(model_file.read(),
                                custom_objects={'SumLayer':SumLayer})
    model.load_weights(weights_path)

    # get the model inputs
    jet_array, subjet_array = preproc_inputs(jets, subjets)
    outputs = model.predict([jet_array, subjet_array])
    return outputs



####################################################################
# Preprocessing and saving the configuration
####################################################################
#

def preproc_inputs(jets, subjets):
    """
    We make some hardcoded transformations to normalize these inputs
    """

    subjet_vars = ['pt', 'rnnip_pb', 'JetFitter_mass']
    subjet_array = np.stack([subjets[x] for x in subjet_vars], axis=2)

    jet_array = np.stack([jets[x] for x in ['pt']], axis=1)
    return jet_array, subjet_array

if __name__ == '__main__':
    run()
